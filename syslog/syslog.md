SYSLOG
=======

### What is SYSLOG ?

Syslog is a way for network devices to send event messages to a logging server – usually known as a Syslog server. The Syslog protocol is supported by a wide range of devices and can be used to log different types of events.

### Services
* ***syslog-ng*** as syslog server

### Configurations

	*Address  : 10.242.0.110
	*Image    : Debian
	*Location : wtc-esx-01

### Configuration File 

The syslog-ng configaration file is /etc/syslog-ng/syslog-ng.conf

### Configuration File Structure

	*Destinations - these are the files where logs end up
	*Filters      - these perform log routing
	*Sources      - where the logs originate
	*Log Paths    - connects sources to destinations through filters

### Add a syslog client

In order to add a client make sure the client is sending to the server source, create a host filter under the heading host filters, create destination files where your file path looks like this...

	/var/log/<hostname>/${FACILITY}.${LEVEL}

The macros FACILTY and LEVEL will name each file according to the facility the log was sent from and the level of importance of the log. After that create a log path that joins the source of the log message to its destination using a host filter and any other filters.

### Special Note 
	
	*Create your own folders before you refer to them using destination filters.
	*Other syslog-ng functionalities have been disabled.
	*Syslog-ng is not supported by mac osx platform.
	*[Mac osx syslog clients have to be configured using this page](https://wiki.splunk.com/Community:HowTo_Configure_Mac_OS_X_Syslog_To_Forward_Data)
	
