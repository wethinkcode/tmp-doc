DHCP
====

### What is DHCP?

The Dynamic Host Configuration Protocol (DHCP) is a network protocol used to assign IP addresses and provide configuration information to devices such as servers, desktops, or mobile devices, so they can communicate on a network using the Internet Protocol (IP).

### Configuration

    *Address*  : 10.242.0.100
    *Address*  : 10.205.0.200
    *Image*    : Debian
    *Location* : wtc-esx-01

### Services
* ***isc-dhcp-server*** as DHCP server

### Script are in /etc/scripts
	- check_pack.sh	: Verify DHCPACK to iMacs
	- restart.sh	: Restart isc-dhcp-server service
	- status.sh     : Check isc-dhcp-server status

