ZFS-STUDENT-6
====

### What is ZFS ?

ZFS is a combined file system and logical volume manager designed by Sun Microsystems. The features of ZFS include protection against data corruption, support for high storage capacities, efficient data compression, integration of the concepts of filesystem and volume management, snapshots and copy-on-write clones, continuous integrity checking and automatic repair, RAID-Z and native NFSv4 ACLs.

### Configuration

    *Address*  : 10.242.0.202
    *Address*  : 10.205.0.202
    *Image*    : Debian
    *Location* : wtc-esx-01

### Add a user home

In order to create a home, you need to use tools located in /tank/tools to create a folder with his name, chown with his name. After that, go will need to edit /etc/exports, and add a line like this...

    /tank/users/xlogin    10.0.0.0/8(rw,async,root_squash,no_subtree_check,insecure)

After that, you will need to use `exportfs -ra` to refresh the export file.
And it should be good.

### Script

Everything that you need to use is in /tank.

    /tank/users (all users home)
    /tank/tools (script for creating users home/chown)
    /tank/intracdn (all intracdn files, pdf, videos, etc)

