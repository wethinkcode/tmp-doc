SWITCH
====

### What is a SWITCH ?

A network switch (also called switching hub, bridging hub, officially MAC bridge) is a computer networking device that connects devices together on a computer network, by using packet switching to receive, process and forward data to the destination device. Unlike less advanced network hubs, a network switch forwards data only to one or multiple devices that need to receive it, rather than broadcasting the same data out of each of its ports.

### Configuration sw-bocal

    *Address*  : 10.242.0.1
    *Image*    : No image

### Configuration sw-cluster-5
    
    *Address*  : 10.205.0.250
    *Image*    : No image

### Useful command
