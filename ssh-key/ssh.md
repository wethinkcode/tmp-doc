DHCP
====

### What is SSH ?

Secure Shell, or SSH, is a cryptographic (encrypted) network protocol operating at layer 7 of the OSI Model to allow remote login and other network services to operate securely over an unsecured network.

SSH provides a secure channel over an unsecured network in a client-server architecture, connecting an SSH client application with an SSH server. Common applications include remote command-line login and remote command execution, but any network service can be secured with SSH. The protocol specification distinguishes between two major versions, referred to as SSH-1 and SSH-2.

The most visible application of the protocol is for access to shell accounts on Unix-like operating systems, but it sees some limited use on Windows as well. In 2015, Microsoft announced that they would include native support for SSH in a future release.

### Useful link
* http://www.linuxproblem.org/art_9.html
* http://www.cyberciti.biz/faq/create-ssh-config-file-on-linux-unix/

### Process ssh-key server

    Create and add key
    How to add your ssh key to a server and never type again your passwd...

    Generate your key, save it.
    ssh-keygen -t rsa

    Copy on the server
    ssh-copy-id server@[ip.address.of.server]

    Add your ssh key in the server
    cat ~/.ssh/id_rsa.pub | ssh user@123.45.56.78 "mkdir -p ~/.ssh && cat >>  ~/.ssh/authorized_keys"

    Security modification
    sudo nano /etc/ssh/sshd_config
    PermitRootLogin without-password
    service ssh reload

    Alias
    alias ssh="ssh -i ~/.ssh/SSH_KEY.PUB -l root"
