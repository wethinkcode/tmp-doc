GITLAB
======
### What is gitlab

GitLab is a Rails application providing Git repository management with fine grained access controls, code reviews, issue tracking, activity feeds, wikis and continuous integration. Essentially, it is a way to manage git repositories on a centralized server.

### Services

### Configurations

    *Address*  : 10.242.0.105
    *Image*    : Debian
    *Location* : wtc-esx-01
    *Security* : [This page gives the step by step for the implementation of the ssl certificate installation and internet protocol change from http to https](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/629def0a7a26e7c2326566f0758d4a27857b52a3/doc/settings/nginx.md)

### SSL Certificates are in /etc/gitlab/ssl/
    - gitlab.wtc.co.za.crt  : actual ssl certificate
    - gitlab.wtc.co.za.key  : ssl key

### Special Note
    **please do not touch the gitlab-ci configurations**
    **please do not change the ssl certificate names**
