KERBEROS
=======

### What is the Kerberos protocol?

Kerberos /ˈkərbərɒs/ is a computer network authentication protocol that works on the basis of 'tickets' to allow nodes communicating over a non-secure network to prove their identity to one another in a secure manner. The protocol was named after the character Kerberos (or Cerberus) from Greek mythology, the ferocious three-headed guard dog of Hades (hellhound). Its designers aimed it primarily at a client–server model and it provides mutual authentication—both the user and the server verify each other's identity. Kerberos protocol messages are protected against eavesdropping and replay attacks.

### Services
* ***krb5-admin-server*** as Kerberos 5 Admin Server
* ***krb5-kdc*** as Kerberos 5 Key Distribution Center

### Configurations

	*Address  : 10.242.0.115
	*Image    : Debian
	*Location : wtc-esx-01

### Configuration Files

`/etc/krb5.conf` applies to all applications using the Kerboros library, on clients and servers.

`/etc/krb5kdc/kdc.conf` is used for KDC-specific applications or for specifying additional settings. It gets merged with `krb5.conf` into a configuration profile for use by applications accessing the KDC database directly.

`/etc/krb5kdc/kadm5.acl` is only used on the KDC, it controls permissions for modifying the KDC database.


### Kerberos Principal Management

Start the Kerberos command-line interface:

	kadmin.local


List the available principals:

	listprincs


Add a principal:

	addprinc <principal>


Remove a principal:

	delprinc <principal>


Get principal information:

	getprinc <principal>


Modify a principal:

	modprinc <option> <principal>

### Useful Documentation
[Kerberos Installation on Debian](https://debian-administration.org/article/570/MIT_Kerberos_installation_on_Debian)

[MIT Kerberos Documentation](http://web.mit.edu/kerberos/)


### Special Note 
`kadmin` requires a password for authentication. `kadmin.local` will authenticate as principal root/admin without asking for the password and is only intended to be run on the administration server.


