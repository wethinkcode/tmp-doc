DNS
======
### What is a dns ?

The Domain Name System (DNS) is a hierarchical decentralized naming system for computers, services, or any resource connected to the Internet or a private network. It associates various information with domain names assigned to each of the participating entities. Most prominently, it translates more readily memorized domain names to the numerical IP addresses needed for the purpose of locating and identifying computer services and devices with the underlying network protocols. By providing a worldwide, distributed directory service, the Domain Name System is an essential component of the functionality of the Internet.

### Services
* ***bind9*** as dns service

### Configurations

    *Address*  : 10.242.0.102
    *Image*    : Debian
    *Location* : wtc-esx-01

### File and configuration

All files and configurations are in `/etc/bind/zones/`, you should find 2 files...
the reverse zone allows DNS to convert from an address to a name.

    - /etc/bind/zones/db.wtc.co.za (zone file)
    - /etc/bind/zones/db.242 (reverse zone file)

### Adding in db.wtc.co.za

*Add in the A Record Addresses your new service/machine*

    - example      IN      A    [ip of your machine]


### Adding in db.wtc.co.za

*Add in the A Record Addresses your new service/machine*

    - [three digit of ip]          IN      PTR     [your machine].wtc.co.za.
    (yes, you need to put the dot at the end)

