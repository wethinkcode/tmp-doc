SHUTDOWN
======
### *How to shutdown WeThinkCode_ servers ?*

    WeThinkCode_ server room is composed with multiples elements. At the bottom the ESXi server (all the VMs), after that the ZFS-STUDENT-6, that contain the CDN, Homes, Munki. The bottom switch is the Staff switch. The 3 others are the cluster switchs. At the top you will find the routeur switch. 

### *Keep calm...*
    
    In case of emergency, and if the generator doesn't kick in, you will need to shutdown everything. NEVER CUT THE ELECTRICITY RIGHT FROM THE PLUGS/WALL/WHATEVER.
    Don't panic, you should have around 1 hour of battery with the UPS to have electricity for all the servers/switchs/routeur/camera/KVM.
    Don't panic, if you can, contact Geff or Max as soon as possible. (even if they are far away in holidays)
    Send someone check why does the generator doesn't work (should be) and contact urban ocean. (brent@urbanocean.co.za)

### *... and shutdown ...*

    You will need to shutdown all the virtuals machines one by one. (first one, first to shutdown)
    
        -= First to shutdown =-

        - cctv, virdi, jarvis, gitlab
        - ansible-42
        - clonezilla
        - munki
        - dns
        - dhcp
        - zfs-student-6
        - nat-openbsd
        - ESXi server (when all the VMs are OFF, ONLY !!!)
        - switchs
        - routeur

        -= Last to shutdown =-

### *... with good commands*

    Depending on the VM you try to shutdown, you should use differents commands.
        Debian  -> shutdown
        OpenBSD -> shutdown -p now
        Windows -> just shutdown with the button...

### *Turn on - LIGHT IS BACK*

    First, make sure that everything is shutdown. Check that the UPS is working, and electricity is back. You can turn on slowly the routeur and all the switchs. Wait that they finish to boot (take around 10/15 minutes). 
    Everytime you turn back on a machine, make sure that's it's working well.
    Then follow...
    
        -= First to turn on =-

        - ESXi server
        - zfs-student-6
        - nat-openbsd
        - dhcp
        - dns
        - munki
        - clonezilla
        - cctv, virdi, jarvis, gitlab
        - ansible-42

        -= Last to turn on =-

### *Tips and Tricks*

    Don't shutdown the iMacs. If they all turn off, when the electricity will come back, they should turn back on automatically. If they don't, just use the power button located behind the machine.
    Send an email at least to [all@wethinkcode.co.za] to warn everybody that there was a electricity cut. (or make a @channel on slack)
